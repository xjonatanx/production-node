var MongoClient = require("mongodb").MongoClient;
var url = "mongodb://localhost:27017/";

// USER

//POST - Insert User
exports.addUser = (req, res) => {
  var dbName = "melon";
  var port = "27017";
  var requiredCollection = "usuario";
  var host = "localhost";

  // open the connection the DB server
  MongoClient.connect("mongodb://" + host + ":" + port, function(
    error,
    usuario
  ) {
    var db = usuario.db(dbName);
    if (error) throw error;
    db
      .collection(requiredCollection)
      .createIndex({ rut_usuario: 1 }, { unique: true });
    db
      .collection(requiredCollection)
      .find({})
      .toArray(function(err, result) {
        if (err) throw (err, console.log(err));
        var docs = [
          {
            id_usuario: result[result.length - 1].id_usuario + 1,
            id_empresa_fk: req.body.empresa,
            nombre: req.body.nombre,
            id_cuadrilla_fk: null,
            rut_usuario: req.body.rut,
            cargo: req.body.perfil,
            apellido: req.body.apellido,
            email: req.body.email,
            url_foto: '',
            password: '',
            id_correctiva_fk: 0,
            id_ast_fk: 0
          }
        ];
        db
          .collection(requiredCollection)
          .insertMany(docs, function(error, inserted) {
            if (error) {
              console.error(error);
              if (error.code === 11000) {
                res.json(false);
              }
            } else {
              res.json(true);
            }
            usuario.close();
          });
      });
  });
};

//GET - get user
exports.getUser = (req, res) => {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    dbo
      .collection("usuario")
      .aggregate([
        {
          $lookup: {
            from: "empresa",
            localField: "id_empresa_fk",
            foreignField: "id_empresa",
            as: "usuarioEmpresa"
          }
        },
        {
          $lookup: {
            from: "cuadrilla",
            localField: "id_cuadrilla_fk",
            foreignField: "id_cuadrilla",
            as: "usuarioCuadrilla"
          }
        }
      ])
      .toArray(function(err, result) {
        if (err) throw err;
        res.json(result);
        db.close();
      });
  });
};

//PUT - put user
exports.putUser = (req, res) => {
  var ObjectId = require("mongodb").ObjectID;

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    var myquery = { _id: ObjectId(req.body._id) };
    var newvalues = {
      $set: {
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        rut: req.body.rut,
        cargo: req.body.perfil,
        id_empresa_fk: req.body.empresa,
        email: req.body.email
      }
    };
    dbo
      .collection("usuario")
      .updateOne(myquery, newvalues, function(err, response) {
        if (err) throw (err, console.log(err));
        res.json(true);
        db.close();
      });
  });
};
