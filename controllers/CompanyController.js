var MongoClient = require("mongodb").MongoClient;
var url = "mongodb://localhost:27017/";

// COMPANY

//POST - Insert company
exports.addEmpresa = (req, res) => {
  var dbName = "melon";
  var port = "27017";
  var requiredCollection = "empresa";
  var host = "localhost";

  // open the connection the DB server
  MongoClient.connect("mongodb://" + host + ":" + port, function(
    error,
    empresa
  ) {
    var db = empresa.db(dbName);
    if (error) throw error;
    db.collection(requiredCollection).createIndex({ "nombre": 1, "codigo": 1},{ unique: true })
    db
      .collection(requiredCollection)
      .find({})
      .toArray(function(err, result) {
        if (err) throw (err, console.log(err));
        var docs = [
          {
            id_empresa: result[result.length - 1].id_empresa + 1,
            nombre: req.body.nombre.toLowerCase(),
            codigo: req.body.codigo.toUpperCase(),
            fecha_creacion: new Date()
          }
        ];
        db
          .collection(requiredCollection)
          .insertMany(docs, function(error, inserted) {
            if (error) {
              console.error(error);
              if (error.code === 11000) {
                res.json(false);
              }
            } else {
              res.json(true);
            }
            empresa.close();
          });
      });
  });
};

//GET - get company
exports.getEmpresa = (req, res) => {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    dbo
      .collection("empresa")
      .find({})
      .toArray(function(err, result) {
        if (err) throw (err, console.log(err));
        res.json(result);
        db.close();
      });
  });
};

//PUT - put company
exports.putEmpresa = (req, res) => {
  var ObjectId = require("mongodb").ObjectID;

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    var myquery = { _id: ObjectId(req.body._id) };
    var newvalues = {
      $set: { codigo: req.body.codigo.toUpperCase(), nombre: req.body.nombre.toLowerCase() }
    };
    dbo.collection("empresa").updateOne(myquery, newvalues, function(err, response) {
      if (err) {
        console.log(err)
        if (err.code === 11000) {
          res.json(false);
        }
      } else {
        res.json(true);
      }
      db.close();
    });
  });
};