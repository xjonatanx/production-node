var MongoClient = require("mongodb").MongoClient;
var url = "mongodb://localhost:27017/";

//POST - Insert faena
exports.addFaena = (req, res) => {
  var dbName = "melon";
  var requiredCollection = "faena";

  // open the connection the DB server
  MongoClient.connect(url, function(error, faena) {
    var db = faena.db(dbName);
    if (error) throw error;
    db
      .collection(requiredCollection)
      .find({})
      .toArray(function(err, result) {
        if (err) throw (err, console.log(err));
        var docs = [
          {
            codigo: result[result.length - 1].codigo + 1,
            nombre: req.body[0].nombre,
            fecha_creacion: new Date(),
            fecha_actualizacion: new Date(),
            path_imagen: req.body[1].url,
            origen: '',
            prioridad: '',
            aprobacion: 1,
            sincronizado: 1,
            path_imagen_bn: '',
            estado: 1
          }
        ];
        db
          .collection(requiredCollection)
          .insertMany(docs, function(error, inserted) {
            if (error) {
              console.error(error);
              if (error.code === 11000) {
                res.json(false);
              }
            } else {
              res.json(true);
            }
          });
      });
  });
};

//GET - get faenas
exports.getFaenas = (req, res) => {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    dbo
      .collection("faena")
      .find({ estado: 1 })
      .toArray(function(err, result) {
        if (err) throw (err, console.log(err));
        res.json(result);
        db.close();
      });
  });
};

//POST - Insert equipo
exports.addEquipo = (req, res) => {
  var dbName = "melon";
  var requiredCollection = "equipo";

  // open the connection the DB server
  MongoClient.connect(url, function(error, equipo) {
    var db = equipo.db(dbName);
    db
      .collection(requiredCollection)
      .find({})
      .toArray(function(err, result) {
        if (err) throw (err, console.log(err));
        var docs = [
          {
            id_equipo: result[result.length - 1].id_equipo + 1,
            nombre: req.body[0].nombre,
            fecha_creacion: new Date(),
            fecha_actualizacion: new Date(),
            id_faena_fk: req.body[0].idFaena,
            path_imagen: req.body[1].url,
            path_imagen_bn: '',
            sincronizado: 1,
            estado: 1
          }
        ];
        db
          .collection(requiredCollection)
          .insertMany(docs, function(error, inserted) {
            if (error) {
              console.error(error);
            } else {
              res.json(true);
            }
          });
      });
  });
};

//GET - get equipo
exports.getEquipo = (req, res) => {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    dbo
      .collection("equipo")
      .find({
        id_faena_fk: parseInt(req.query.idFaena),
        estado: 1
      })
      .toArray(function(err, result) {
        if (err) throw (err, console.log(err));
        res.json(result);
        db.close();
      });
  });
};

//POST - Insert actividad
exports.addActividad = (req, res) => {
  var dbName = "melon";
  var requiredCollection = "actividad";

  // open the connection the DB server
  MongoClient.connect(url, function(error, actividad) {
    var db = actividad.db(dbName);
    if (error) throw error;
    db
      .collection(requiredCollection)
      .find({})
      .toArray(function(err, result) {
        if (err) throw (err, console.log(err));
        var docs = [
          {
            id_actividad: result[result.length - 1].id_actividad + 1,
            nombre: req.body.nombre,
            fecha_creacion: new Date(),
            fecha_actualizacion: new Date(),
            id_equipo_fk: req.body.idEquipo,
            sincronizado: 1,
            estado: 1
          }
        ];
        db
          .collection(requiredCollection)
          .insertMany(docs, function(error, inserted) {
            if (error) {
              console.error(error);
            } else {
              res.json(true);
            }
          });
      });
  });
};

//GET - get actividad
exports.getActividad = (req, res) => {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    dbo
      .collection("actividad")
      .find({
        id_equipo_fk: parseInt(req.query.idEquipo),
        estado: 1
      })
      .toArray(function(err, result) {
        if (err) throw (err, console.log(err));
        res.json(result);
        db.close();
      });
  });
};

//POST - Insert peligro
exports.addPeligro = (req, res) => {
  var dbName = "melon";
  var requiredCollection = "peligro";

  // open the connection the DB server
  MongoClient.connect(url, function(error, peligro) {
    var db = peligro.db(dbName);
    if (error) throw error;
    db
      .collection(requiredCollection)
      .find({})
      .toArray(function(err, result) {
        if (err) throw (err, console.log(err));
        var docs = [
          {
            id_peligro: result[result.length - 1].id_peligro + 1,
            nombre: req.body.nombre,
            fecha_creacion: new Date(),
            fecha_actualizacion: new Date(),
            id_actividad_fk: req.body.idActividad,
            estado: 1,
            fake: 0,
            sincronizado: 1,
          }
        ];
        db
          .collection(requiredCollection)
          .insertMany(docs, function(error, inserted) {
            if (error) {
              console.error(error);
            } else {
              res.json(true);
            }
          });
      });
  });
};

//GET - get peligros
exports.getPeligro = (req, res) => {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    dbo
      .collection("peligro")
      .find({
        id_actividad_fk: parseInt(req.query.idActividad),
        estado: 1
      })
      .toArray(function(err, result) {
        if (err) throw (err, console.log(err));
        res.json(result);
        db.close();
      });
  });
};

//POST - Insert medida control
exports.addMedidaControl = (req, res) => {
  var dbName = "melon";
  var requiredCollection = "medidacontrol";

  // open the connection the DB server
  MongoClient.connect(url, function(error, medidacontrol) {
    var db = medidacontrol.db(dbName);
    if (error) throw error;
    db
      .collection(requiredCollection)
      .find({})
      .toArray(function(err, result) {
        if (err) throw (err, console.log(err));
        var docs = [
          {
            id_medida: result[result.length - 1].id_medida + 1,
            nombre: req.body.nombre,
            fecha_creacion: new Date(),
            fecha_actualizacion: new Date(),
            accion_correctiva: '',
            fk_user_responsable: 0,
            id_peligro_fk: req.body.idPeligro,
            estado: 1,
            fake: 0,
            sincronizado: 0
          }
        ];
        db
          .collection(requiredCollection)
          .insertMany(docs, function(error, inserted) {
            if (error) {
              console.error(error);
            } else {
              res.json(true);
            }
          });
      });
  });
};

//GET - get medidas de control
exports.getMedidaControl = (req, res) => {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    dbo
      .collection("medidacontrol")
      .find({
        id_peligro_fk: parseInt(req.query.idPeligro),
        estado: 1
      })
      .toArray(function(err, result) {
        if (err) throw (err, console.log(err));
        res.json(result);
        db.close();
      });
  });
};

//PUT - put faena
exports.putFaena = (req, res) => {
  var ObjectId = require("mongodb").ObjectID;

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    var myquery = { _id: ObjectId(req.body._id) };
    var newvalues = {
      $set: {
        nombre: req.body.nombre,
        fecha_actualizacion: new Date()
      }
    };
    dbo
      .collection("faena")
      .updateOne(myquery, newvalues, function(err, response) {
        if (err) {
          console.log(err);
          if (err.code === 11000) {
            res.json(false);
          }
        } else {
          res.json(true);
        }
        db.close();
      });
  });
};

//PUT - put equipo
exports.putEquipo = (req, res) => {
  var ObjectId = require("mongodb").ObjectID;

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    var myquery = { _id: ObjectId(req.body._id) };
    var newvalues = {
      $set: {
        nombre: req.body.nombre,
        fecha_actualizacion: new Date()
      }
    };
    dbo.collection("equipo").updateOne(myquery, newvalues, function(err, res) {
      if (err) throw (err, console.log(err));
      db.close();
    });
    res.json(true);
  });
};

//PUT - put actividad
exports.putActividad = (req, res) => {
  var ObjectId = require("mongodb").ObjectID;

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    var myquery = { _id: ObjectId(req.body._id) };
    var newvalues = {
      $set: {
        nombre: req.body.nombre,
        fecha_actualizacion: new Date()
      }
    };
    dbo
      .collection("actividad")
      .updateOne(myquery, newvalues, function(err, res) {
        if (err) throw (err, console.log(err));
        db.close();
      });
    res.json(true);
  });
};

//PUT - put peligro
exports.putPeligro = (req, res) => {
  var ObjectId = require("mongodb").ObjectID;

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    var myquery = { _id: ObjectId(req.body._id) };
    var newvalues = {
      $set: {
        nombre: req.body.nombre,
        fecha_actualizacion: new Date()
      }
    };
    dbo.collection("peligro").updateOne(myquery, newvalues, function(err, res) {
      if (err) throw (err, console.log(err));
      db.close();
    });
    res.json(true);
  });
};

//PUT - put medida de control
exports.putMedidaControl = (req, res) => {
  var ObjectId = require("mongodb").ObjectID;

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    var myquery = { _id: ObjectId(req.body._id) };
    var newvalues = {
      $set: {
        nombre: req.body.nombre,
        fecha_actualizacion: new Date()
      }
    };
    dbo
      .collection("medidacontrol")
      .updateOne(myquery, newvalues, function(err, res) {
        if (err) throw (err, console.log(err));
        db.close();
      });
    res.json(true);
  });
};

//PUT - DELETE FAENA
exports.cambiarEstadoFaena = (req, res) => {
  var ObjectId = require("mongodb").ObjectID;

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    var myquery = { _id: ObjectId(req.body._id) };
    var newvalues = {
      $set: {
        estado: 0
      }
    };
    dbo.collection("faena").updateOne(myquery, newvalues, function(err, res) {
      if (err) throw (err, console.log(err));
      db.close();
    });
    res.json(true);
  });
};

//PUT - DELETE EQUIPOS
exports.cambiarEstadoEquipo = (req, res) => {
  var ObjectId = require("mongodb").ObjectID;

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    var myquery = { _id: ObjectId(req.body._id) };
    var newvalues = {
      $set: {
        estado: 0
      }
    };
    dbo.collection("equipo").updateOne(myquery, newvalues, function(err, res) {
      if (err) throw (err, console.log(err));
      db.close();
    });
    res.json(true);
  });
};

//PUT - DELETE ACTIVIDADES
exports.cambiarEstadoActividad = (req, res) => {
  var ObjectId = require("mongodb").ObjectID;

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    var myquery = { _id: ObjectId(req.body._id) };
    var newvalues = {
      $set: {
        estado: 0
      }
    };
    dbo
      .collection("actividad")
      .updateOne(myquery, newvalues, function(err, res) {
        if (err) throw (err, console.log(err));
        db.close();
      });
    res.json(true);
  });
};

//PUT - DELETE PELIGRO
exports.cambiarEstadoPeligro = (req, res) => {
  var ObjectId = require("mongodb").ObjectID;

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    var myquery = { _id: ObjectId(req.body._id) };
    var newvalues = {
      $set: {
        estado: 0
      }
    };
    dbo.collection("peligro").updateOne(myquery, newvalues, function(err, res) {
      if (err) throw (err, console.log(err));
      db.close();
    });
    res.json(true);
  });
};

//PUT - DELETE MEDIDA DE CONTROL
exports.cambiarEstadoMedidaControl = (req, res) => {
  var ObjectId = require("mongodb").ObjectID;

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    var myquery = { _id: ObjectId(req.body._id) };
    var newvalues = {
      $set: {
        estado: 0
      }
    };
    dbo
      .collection("medidacontrol")
      .updateOne(myquery, newvalues, function(err, res) {
        if (err) throw (err, console.log(err));
        db.close();
      });
    res.json(true);
  });
};

//PUT - put medida de control fake
exports.changeFakeMedidaControl = (req, res) => {
  var ObjectId = require("mongodb").ObjectID;

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    var myquery = { _id: ObjectId(req.body._id) };
    var newvalues = {
      $set: {
        fake: req.body.fake === 0 ? 1 : 0,
        fecha_actualizacion: new Date()
      }
    };
    dbo
      .collection("medidacontrol")
      .updateOne(myquery, newvalues, function(err, res) {
        if (err) throw (err, console.log(err));
        db.close();
      });
    res.json(true);
  });
};

//PUT - put peligro fake
exports.changeFakePeligro = (req, res) => {
  var ObjectId = require("mongodb").ObjectID;

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    var myquery = { _id: ObjectId(req.body._id) };
    var newvalues = {
      $set: {
        fake: req.body.fake === 0 ? 1 : 0,
        fecha_actualizacion: new Date()
      }
    };
    dbo.collection("peligro").updateOne(myquery, newvalues, function(err, res) {
      if (err) throw (err, console.log(err));
      db.close();
    });
    res.json(true);
  });
};
