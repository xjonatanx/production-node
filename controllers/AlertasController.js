var MongoClient = require("mongodb").MongoClient;
var url = "mongodb://localhost:27017/";

// ALERTAS

//POST - Insert alert
exports.addAlerta = (req, res) => {
  var dbName = "melon";
  var port = "27017";
  var requiredCollection = "alerta";
  var host = "localhost";

  // open the connection the DB server
  MongoClient.connect("mongodb://" + host + ":" + port, function(
    error,
    alerta
  ) {
    var db = alerta.db(dbName);
    if (error) throw error;
    db
      .collection(requiredCollection)
      .find({})
      .toArray(function(err, result) {
        if (err) throw (err, console.log(err));
        var docs = [
          {
            id_alerta: result[result.length - 1].id_alerta + 1,
            fecha_creacion: new Date(),
            fecha_actualizacion: new Date(),
            gravedad: parseInt(req.body.gravedad),
            mensaje: req.body.mensaje,
            tipo: req.body.tipo,
            estado: 1
          }
        ];
        db
          .collection(requiredCollection)
          .insertMany(docs, function(error, inserted) {
            if (error) {
              console.error(error);
            } else {
              res.json(true);
            }
            alerta.close();
          });
      });
  });
};

//GET - get alerta
exports.getAlertas = (req, res) => {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    dbo
      .collection("alerta")
      .find({})
      .toArray(function(err, result) {
        if (err) throw (err, console.log(err));
        res.json(result);
        db.close();
      });
  });
};

//PUT - desactivar alerta
exports.desactivarAlerta = (req, res) => {
  var ObjectId = require("mongodb").ObjectID;

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    var myquery = { _id: ObjectId(req.body._id) };
    var newvalues = {
      $set: { 
        estado: 0,
        fecha_actualizacion: new Date()
      }
    };
    dbo
      .collection("alerta")
      .updateOne(myquery, newvalues, function(err, response) {
        if (err) {
          console.log(err);
        } else {
          res.json(true);
        }
        db.close();
      });
  });
};

exports.deleteAlerta = (req, res) => {
  var ObjectId = require("mongodb").ObjectID;
  MongoClient.connect(url, function(err, db) {
    if (err) throw (err, console.log(err));
    var dbo = db.db("melon");
    var myquery = { _id: ObjectId(req.query._id) };
    dbo.collection("alerta").deleteOne(myquery, function(err, obj) {
      if (err) throw (err, console.log(err));
      res.json(true)
      db.close();
    });
  });
};
