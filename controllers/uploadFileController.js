// Upload File
exports.uploadFile = (req, res) => {
  var express = require("express"),
    http = require("http"),
    formidable = require("formidable"),
    fs = require("fs"),
    path = require("path");

  var app = express();

  var form = new formidable.IncomingForm();
  form.parse(req, function(err, fields, files) {
    var old_path = files.file.path,
      file_size = files.file.size,
      file_ext = files.file.name.split(".").pop(),
      index = old_path.lastIndexOf("/") + 1,
      file_name = old_path.substr(index),
      new_path = path.join(
        process.env.PWD,
        "/temp/",
        file_name + "." + file_ext
      );

    fs.readFile(old_path, function(err, data) {
      fs.writeFile(new_path, data, function(err) {
        fs.unlink(old_path, function(err) {
          if (err) {
            res.status(500);
            res.json({ success: false });
          } else {
            const keyFilename =
              "./ast-melon-792f1-firebase-adminsdk-1lmas-cd305c7d74.json";
            const projectId = "ast-melon-792f1";
            const bucketName = `${projectId}.appspot.com`;
            var MimeLookup = require("mime-lookup");
            var mime = new MimeLookup(require("mime-db"));

            const gcs = require("@google-cloud/storage")({
              projectId,
              keyFilename
            });

            const bucket = gcs.bucket(bucketName);

            const filePath = new_path;
            const uploadTo = `melon-faenas/`+ new Date + files.file.name;
            const fileMime = mime.lookup(filePath);

            bucket.upload(
              filePath,
              {
                destination: uploadTo,
                public: true,
                metadata: {
                  contentType: fileMime,
                  cacheControl: "public, max-age=300"
                }
              },
              function(err, file) {
                if (err) {
                  console.log(err);
                  return;
                }
                fs.unlinkSync(new_path);
                res.status(200);
                res.json({ success: true, url: createPublicFileURL(uploadTo) });
              }
            );

            function createPublicFileURL(storageName) {
              return `http://storage.googleapis.com/${bucketName}/${encodeURIComponent(
                storageName
              )}`;
            }
          }
        });
      });
    });
  });
};
