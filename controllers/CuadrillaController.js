var MongoClient = require("mongodb").MongoClient;
var url = "mongodb://localhost:27017/";

//CUADRILLA

//GET - OBTENER TODOS LOS SUPERVISORES Y OPERADORES QUE NO TENGAN CUADRILLA
exports.getSupervisorOperadorNull = (req, res) => {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    dbo
      .collection("usuario")
      .find({
        id_empresa_fk: parseInt(req.query.company_id),
        cargo: "Supervisor",
        id_cuadrilla_fk: null
      })
      .toArray(function(err, supervisores) {
        if (err) throw (err, console.log(err));
        MongoClient.connect(url, function(err, db) {
          if (err) throw err;
          var dbo = db.db("melon");
          dbo
            .collection("usuario")
            .find({
              id_empresa_fk: parseInt(req.query.company_id),
              cargo: "Operador",
              id_cuadrilla_fk: null
            })
            .toArray(function(err, operadores) {
              if (err) throw (err, console.log(err));
              var operadorSupervisor = [];
              operadorSupervisor.push({ supervisor: supervisores });
              operadorSupervisor.push({ operador: operadores });
              res.json(operadorSupervisor);
            });
          db.close();
        });
      });
    db.close();
  });
};

// OBTENER TODOS LOS SUPERVISORES, OBTENER TODOS LOS OPERADORES DE UNA EMPRESA CON CUADRILLA ASIGNADA Y SIN CUADRILLA ASIGNADA
exports.getDataAllSupervisorOperador = (req, res) => {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    dbo
      .collection("usuario")
      .find({
        id_empresa_fk: parseInt(req.query.company_id),
        cargo: "Supervisor"
      })
      .toArray(function(err, supervisores) {
        if (err) throw (err, console.log(err));
        MongoClient.connect(url, function(err, db) {
          if (err) throw err;
          var dbo = db.db("melon");
          dbo
            .collection("usuario")
            .find({
              id_empresa_fk: parseInt(req.query.company_id),
              cargo: "Operador",
              id_cuadrilla_fk: parseInt(req.query.id_cuadrilla)
            })
            .toArray(function(err, operadores) {
              if (err) throw (err, console.log(err));
              MongoClient.connect(url, function(err, db) {
                if (err) throw err;
                var dbo = db.db("melon");
                dbo
                  .collection("usuario")
                  .find({
                    id_empresa_fk: parseInt(req.query.company_id),
                    cargo: "Operador",
                    id_cuadrilla_fk: null
                  })
                  .toArray(function(err, operadoresSinCuadrilla) {
                    if (err) throw (err, console.log(err));
                    var operadorSupervisores = [];
                    operadorSupervisores.push({ supervisor: supervisores });
                    operadorSupervisores.push({
                      operadores_cuadrilla: operadores
                    });
                    operadorSupervisores.push({
                      operadores_sin_cuadrilla: operadoresSinCuadrilla
                    });
                    res.json(operadorSupervisores);
                  });
              });
              db.close();
            });
          db.close();
        });
      });
    db.close();
  });
};

//POST - Insert Cuadrilla
exports.addCuadrilla = (req, res) => {
  var port = "27017";
  var host = "localhost";
  var dbName = "melon";
  var requiredCollection = "cuadrilla";

  MongoClient.connect(url, function(error, cuadrilla) {
    var db = cuadrilla.db(dbName);
    if (error) throw error;
    db
      .collection(requiredCollection)
      .createIndex({ nombre: 1, sigla: 1 }, { unique: true });
    db
      .collection(requiredCollection)
      .find({})
      .toArray(function(err, result) {
        if (err) throw (err, console.log(err));
        var docs = [
          {
            id_cuadrilla: result[result.length - 1].id_cuadrilla + 1,
            fecha_creacion: new Date(),
            fecha_actualizacion: new Date(),
            nombre: req.body.nombre.toLowerCase(),
            sigla: req.body.sigla.toUpperCase(),
            fk_id_supervisor: req.body.supervisor,
            id_empresa_fk: req.body.empresa
          }
        ];
        db
          .collection(requiredCollection)
          .insertMany(docs, function(error, inserted) {
            if (error) {
              console.error(error);
              if (error.code === 11000) {
                res.json(false);
                cuadrilla.close();
              }
            } else {
              MongoClient.connect(url, function(err, db) {
                if (err) throw err;
                var dbo = db.db("melon");
                req.body.operadores.push(req.body.supervisor);
                for (let i = 0; i < req.body.operadores.length; i++) {
                  var myquery = { id_usuario: req.body.operadores[i] };
                  var newvalues = {
                    $set: { id_cuadrilla_fk: result.length + 1 }
                  };
                  dbo
                    .collection("usuario")
                    .updateOne(myquery, newvalues, function(err, res) {
                      if (err) throw err;
                      db.close();
                      cuadrilla.close();
                    });
                }
                res.json(true);
              });
            }
          });
      });
  });
};

//GET - get cuadrilla
exports.getCuadrilla = (req, res) => {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    dbo
      .collection("cuadrilla")
      .aggregate([
        {
          $lookup: {
            from: "usuario",
            localField: "fk_id_supervisor",
            foreignField: "id_usuario",
            as: "cuadrillaUsuario"
          }
        },
        {
          $lookup: {
            from: "empresa",
            localField: "id_empresa_fk",
            foreignField: "id_empresa",
            as: "cuadrillaEmpresa"
          }
        }
      ])
      .toArray(function(err, result) {
        if (err) throw err;
        res.json(result);
        db.close();
      });
  });
};

//PUT - CAMBIAR ID_CUADRILLA_FK DE TODA LA CUADRILLA SELECCIONADA A NULL
exports.putCuadrillaToNull = (req, res) => {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    var myquery = { id_cuadrilla_fk: req.body.id_cuadrilla, cargo: "Operador" };
    var newvalues = {
      $set: { id_cuadrilla_fk: null }
    };
    dbo
      .collection("usuario")
      .updateMany(myquery, newvalues, function(err, response) {
        if (err) throw (err, console.log(err));
        db.close();
      });
    res.json(true);
  });
};

//PUT - ASIGNAR CUADRILLA A OPERADORES SELECCIONADOS
exports.asignarCuadrilla = (req, res) => {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    for (let i = 0; i < req.body["operadores"].length; i++) {
      var queryIdUsuario = { id_usuario: req.body["operadores"][i] };
      var setIdCuadrilla = {
        $set: { id_cuadrilla_fk: req.body.id_cuadrilla }
      };
      var queryFechaUpdateCuadrilla = { id_cuadrilla: req.body.id_cuadrilla };
      var setUpdateFechaActualizacion = {
        $set: { fecha_actualizacion: new Date() }
      };
      dbo
        .collection("usuario")
        .update(queryIdUsuario, setIdCuadrilla, function(err, response) {
          if (err) throw (err, console.log(err));
          db.close();
        });
      dbo
      .collection("cuadrilla")
      .update(queryFechaUpdateCuadrilla, setUpdateFechaActualizacion, function(err, response) {
        if (err) throw (err, console.log(err));
        db.close();
      });
    }
    res.json(true);
  });
};
