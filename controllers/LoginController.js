var MongoClient = require("mongodb").MongoClient;
var url = "mongodb://localhost:27017/";

// vallidate login
exports.validarLogin = (req, res) => {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("melon");
    dbo
      .collection("admin")
      .find({
        rut_admin: req.query.rut,
        password: req.query.password
      })
      .toArray(function(err, result) {
        if (err) throw (err, console.log(err));
        if (result.length > 0) {
          res.json(true);
        } else {
          res.json(false);
        }
        db.close();
      });
  });
};