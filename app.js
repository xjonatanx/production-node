var express = require("express");
var app = express();
const bodyParser = require("body-parser");
var MongoClient = require("mongodb").MongoClient,
  format = require("util").format;
const CompanyCtrl = require("./controllers/CompanyController");
const UserCtrl = require("./controllers/UserController");
const CuadrillaCtrl = require("./controllers/CuadrillaController");
const MatrizCtrl = require("./controllers/MatrizController");
const LoginCtrl = require("./controllers/LoginController");
const AlertCtrl = require("./controllers/AlertasController");
const UploadCtrl = require("./controllers/uploadFileController");
const port = process.env.PORT || 2000;
var app = express();
var cors = require("cors");
app.use(cors());

app.use(express.static(__dirname + "/views"));

// Middlewares
app.use(bodyParser.json()); //for request application/json
app.use(bodyParser.urlencoded({ extended: false }));

app.get("/", (req, res) => {
  res.render("index.html");
});

var router = express.Router();

router.route("/addEmpresa").post(CompanyCtrl.addEmpresa);
router.route("/getEmpresas").get(CompanyCtrl.getEmpresa);
router.route("/putEmpresas").put(CompanyCtrl.putEmpresa);
router.route("/addUser").post(UserCtrl.addUser);
router.route("/getUser").get(UserCtrl.getUser);
router.route("/putUser").put(UserCtrl.putUser);
router.route("/getSupervisorOperadorNull").get(CuadrillaCtrl.getSupervisorOperadorNull);
router.route("/getDataAllSupervisorOperador").get(CuadrillaCtrl.getDataAllSupervisorOperador);
router.route("/addCuadrilla").post(CuadrillaCtrl.addCuadrilla);
router.route("/getCuadrilla").get(CuadrillaCtrl.getCuadrilla);
router.route("/putCuadrillaToNull").put(CuadrillaCtrl.putCuadrillaToNull);
router.route("/asignarCuadrilla").put(CuadrillaCtrl.asignarCuadrilla);
router.route("/addFaena").post(MatrizCtrl.addFaena);
router.route("/getFaenas").get(MatrizCtrl.getFaenas);
router.route("/addEquipo").post(MatrizCtrl.addEquipo);
router.route("/getEquipo").get(MatrizCtrl.getEquipo);
router.route("/addActividad").post(MatrizCtrl.addActividad);
router.route("/getActividad").get(MatrizCtrl.getActividad);
router.route("/addPeligro").post(MatrizCtrl.addPeligro);
router.route("/getPeligro").get(MatrizCtrl.getPeligro);
router.route("/addMedidaControl").post(MatrizCtrl.addMedidaControl);
router.route("/getMedidaControl").get(MatrizCtrl.getMedidaControl);
router.route("/putFaena").put(MatrizCtrl.putFaena);
router.route("/cambiarEstadoFaena").put(MatrizCtrl.cambiarEstadoFaena);
router.route("/cambiarEstadoEquipo").put(MatrizCtrl.cambiarEstadoEquipo);
router.route("/putEquipo").put(MatrizCtrl.putEquipo);
router.route("/putActividad").put(MatrizCtrl.putActividad);
router.route("/putPeligro").put(MatrizCtrl.putPeligro);
router.route("/cambiarEstadoActividad").put(MatrizCtrl.cambiarEstadoActividad);
router.route("/putMedidaControl").put(MatrizCtrl.putMedidaControl);
router.route("/changeFakeMedidaControl").put(MatrizCtrl.changeFakeMedidaControl);
router.route("/changeFakePeligro").put(MatrizCtrl.changeFakePeligro);
router.route("/cambiarEstadoPeligro").put(MatrizCtrl.cambiarEstadoPeligro);
router.route("/cambiarEstadoMedidaControl").put(MatrizCtrl.cambiarEstadoMedidaControl);
router.route("/validarLogin").get(LoginCtrl.validarLogin);
router.route("/addAlerta").post(AlertCtrl.addAlerta);
router.route("/getAlertas").get(AlertCtrl.getAlertas);
router.route("/desactivarAlerta").put(AlertCtrl.desactivarAlerta);
router.route("/deleteAlerta").delete(AlertCtrl.deleteAlerta);
router.route("/uploadFile").post(UploadCtrl.uploadFile);

app.use("/api", router);

app.listen(port, () => {
  console.log(`Server running in http://localhost:${port}/`);
});
